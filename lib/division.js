"use strict";
var path = require("path");

var yeoman = require('yeoman-environment');
var env = yeoman.createEnv();

module.exports = function () {
    env.register(require.resolve("./generators/division"), "ns");
    env.run("ns");
};