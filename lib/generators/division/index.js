'use strict';
var yeoman = require('yeoman-generator');

module.exports = yeoman.Base.extend({
    constructor: function () {
        yeoman.Base.apply(this, arguments);
    },
    initializing: function () {
        this.log("initializing");
        this.props = {};
    },
    prompting: function () {
        var prompts = [{
            type: "input",
            name: "name",
            message: "请指定师的番号？",
            default: "index"
        }];
        return this.prompt(prompts).then(function (answers) {
            this.props.designation = answers.name;
        }.bind(this));
    },
    configuring: function () {
        this.log("configuring");
    },
    default: function () {
        this.log("default");
    },
    writing: function () {
        this.log("writing");
        this.destinationRoot("src");
        this.fs.copy(this.templatePath("project"), this.destinationPath(this.props.designation));
    },
    conflicts: function () {
        this.log("conflicts");
    },
    end: function () {
        this.log("\n师级单位组建完成\n");
    }
});