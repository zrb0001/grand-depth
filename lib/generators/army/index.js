'use strict';
var path = require("path");

var yeoman = require('yeoman-generator');

module.exports = yeoman.Base.extend({
    constructor: function () {
        yeoman.Base.apply(this, arguments);
    },
    initializing: function () {
        this.log("initializing");
        this.props = {};
    },
    prompting: function () {
        var prompts = [{
            type: "input",
            name: "name",
            message: "请指定部队的驻扎地？",
            default: "index"
        }, {
            type: "list",
            name: "mode",
            message: "选择驻扎模式",
            choices: ["flat", "folder"],
            default: 0
        }];
        return this.prompt(prompts).then(function (answers) {
            this.props.designation = answers.name;
            this.props.mode = answers.mode;
        }.bind(this));
    },
    configuring: function () {
        this.log("configuring");
    },
    default: function () {
        this.log("default");
    },
    writing: function () {
        this.log("writing");
        if (this.props.mode === "flat") {
            this.fs.copy(this.templatePath("project"), this.destinationPath("./"));
            // this.log(this.destinationPath("./"));
        } else {
            // this.fs.copy(this.templatePath("project"), this.destinationPath(this.props.designation));
        }
        // this.fs.copy(this.templatePath("project"), this.destinationPath(this.props.designation));
    },
    conflicts: function () {
        this.log("conflicts");
    },
    end: function () {
        this.log("\n部队组建完成\n");
    }
});