'use strict';
var program = require('commander');

var army = require("./army");
var division = require("./division");

program.version("0.0.1");

program.command("army")
    .description("组建军部")
    .action(function () {
        //TODO 组建军部
        army();
    });

program.command("division")
    .description("组件师级作战单位")
    .action(function () {
        division();
    });

program.command("train")
    .description("本地调试");

program.command("battle")
    .description("编译生产");

program.parse(process.argv);